<?php

namespace App\Entity;

use App\Repository\HomeRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=HomeRepository::class)
 * @Vich\Uploadable ()
 */
class Home
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $logo;

    /**
     * @Assert\Image(maxWidth="600")
     * @Vich\UploadableField(mapping="home", fileNameProperty="logo")
     */
    private $logoFile;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $logoAlt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $viewOne;

    /**
     * @Assert\Image(maxWidth="1800")
     * @Vich\UploadableField(mapping="home", fileNameProperty="viewOne")
     */
    private $viewOneFile;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $viewOneAlt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $viewTwo;

    /**
     * @Assert\Image(maxWidth="1080", maxHeight="1080")
     * @Vich\UploadableField (mapping="home", fileNameProperty="viewTwo")
     */
    private $viewTwoFile;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $viewTwoAlt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $viewThree;

    /**
     * @Assert\Image(maxWidth="790", maxHeight="1080")
     * @Vich\UploadableField (mapping="home", fileNameProperty="viewThree")
     */
    private $viewThreeFile;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $viewThreeAlt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getLogoAlt(): ?string
    {
        return $this->logoAlt;
    }

    public function setLogoAlt(string $logoAlt): self
    {
        $this->logoAlt = $logoAlt;

        return $this;
    }

    public function getViewOne(): ?string
    {
        return $this->viewOne;
    }

    public function setViewOne(?string $viewOne): self
    {
        $this->viewOne = $viewOne;

        return $this;
    }

    public function getViewOneAlt(): ?string
    {
        return $this->viewOneAlt;
    }

    public function setViewOneAlt(string $viewOneAlt): self
    {
        $this->viewOneAlt = $viewOneAlt;

        return $this;
    }

    public function getViewTwo(): ?string
    {
        return $this->viewTwo;
    }

    public function setViewTwo(?string $viewTwo): self
    {
        $this->viewTwo = $viewTwo;

        return $this;
    }

    public function getViewTwoAlt(): ?string
    {
        return $this->viewTwoAlt;
    }

    public function setViewTwoAlt(string $viewTwoAlt): self
    {
        $this->viewTwoAlt = $viewTwoAlt;

        return $this;
    }

    public function getViewThree(): ?string
    {
        return $this->viewThree;
    }

    public function setViewThree(?string $viewThree): self
    {
        $this->viewThree = $viewThree;

        return $this;
    }

    public function getViewThreeAlt(): ?string
    {
        return $this->viewThreeAlt;
    }

    public function setViewThreeAlt(string $viewThreeAlt): self
    {
        $this->viewThreeAlt = $viewThreeAlt;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLogoFile()
    {
        return $this->logoFile;
    }

    /**
     * @param mixed $logoFile
     * @throws Exception
     */
    public function setLogoFile(?File $logoFile): void
    {
        $this->logoFile = $logoFile;
        if ($logoFile) {
            $this->updatedAt = new DateTime();
        }
    }

    /**
     * @return mixed
     */
    public function getViewOneFile()
    {
        return $this->viewOneFile;
    }

    /**
     * @param mixed $viewOneFile
     * @throws Exception
     */
    public function setViewOneFile(?File $viewOneFile): void
    {
        $this->viewOneFile = $viewOneFile;
        if ($viewOneFile) {
            $this->updatedAt = new DateTime();
        }
    }

    /**
     * @return mixed
     */
    public function getViewTwoFile()
    {
        return $this->viewTwoFile;
    }

    /**
     * @param mixed $viewTwoFile
     * @throws Exception
     */
    public function setViewTwoFile(?File $viewTwoFile): void
    {
        $this->viewTwoFile = $viewTwoFile;
        if ($viewTwoFile) {
            $this->updatedAt = new DateTime();
        }
    }

    /**
     * @return mixed
     */
    public function getViewThreeFile()
    {
        return $this->viewThreeFile;
    }

    /**
     * @param mixed $viewThreeFile
     * @throws Exception
     */
    public function setViewThreeFile(?File $viewThreeFile): void
    {
        $this->viewThreeFile = $viewThreeFile;
        if ($viewThreeFile) {
            $this->updatedAt = new DateTime();
        }
    }
}
