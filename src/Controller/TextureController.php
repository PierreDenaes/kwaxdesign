<?php

namespace App\Controller;

use App\Repository\HomeRepository;
use App\Repository\TextureRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class TextureController extends AbstractController
{
    /**
     * @Route("/texture", name="personnalisation")
     * @param Environment $twig
     * @param TextureRepository $textureRepository
     * @param HomeRepository $homeRepository
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function index(Environment $twig, TextureRepository $textureRepository, HomeRepository $homeRepository)
    {
        return new Response($twig->render('texture/index.html.twig', ['textures' => $textureRepository->findAll(), 'homes' => $homeRepository->findAll()]));
    }


}
