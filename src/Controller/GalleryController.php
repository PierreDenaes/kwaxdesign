<?php

namespace App\Controller;

use App\Repository\GalleryRepository;
use App\Repository\HomeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class GalleryController extends AbstractController
{
    /**
     * @Route("/gallery", name="gallery")
     * @param Environment $twig
     * @param GalleryRepository $galleryRepository
     * @param HomeRepository $homeRepository
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function index(Environment $twig, GalleryRepository $galleryRepository, HomeRepository $homeRepository)
    {
        return new Response($twig->render('gallery/gallery.html.twig', ['galleries' => $galleryRepository->findAll(), 'homes' => $homeRepository->findAll()]));
    }
}
