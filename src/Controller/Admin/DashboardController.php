<?php

namespace App\Controller\Admin;



use App\Entity\Gallery;
use App\Entity\Home;
use App\Entity\Texture;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('KWax');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Théme Accueil','fas fa-desktop', Home::class);
        yield MenuItem::linkToCrud('Textures', 'fab fa-buromobelexperte', Texture::class);
        yield MenuItem::linkToCrud('Gallerie', 'fab fa-buromobelexperte', Gallery::class);

    }
}
