<?php

namespace App\Controller\Admin;

use App\Entity\Home;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Vich\UploaderBundle\Form\Type\VichImageType;

class HomeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Home::class;
    }

    public function configureFields(string $pageName): iterable
    {
        $logoFile = ImageField::new('logoFile')->setFormType(VichImageType::class);
        $logo = ImageField::new('logo')->setBasePath('/images/home');
        $viewOneFile = ImageField::new('viewOneFile')->setFormType(VichImageType::class);
        $viewOne = ImageField::new('viewOne')->setBasePath('/images/home');
        $viewTwoFile = ImageField::new('viewTwoFile')->setFormType(VichImageType::class);
        $viewTwo = ImageField::new('viewTwo')->setBasePath('/images/home');
        $viewThreeFile = ImageField::new('viewThreeFile')->setFormType(VichImageType::class);
        $viewThree = ImageField::new('viewThree')->setBasePath('/images/home');

        $fields = [
            TextField::new('name', 'Nom'),
            TextField::new('logoAlt', 'Alt logo'),
            TextField::new('viewOneAlt', 'Alt img 1'),
            TextField::new('viewTwoAlt', 'Alt img 2'),
            TextField::new('viewThreeAlt', 'Alt img 3'),
            BooleanField::new('isActive')
        ];

        if ($pageName == Crud::PAGE_INDEX || $pageName == Crud::PAGE_DETAIL){
            $fields[] = $logo;
            $fields[] = $viewOne;
            $fields[] = $viewTwo;
            $fields[] = $viewThree;
        } else {
            $fields[] = $logoFile;
            $fields[] = $viewOneFile;
            $fields[] = $viewTwoFile;
            $fields[] = $viewThreeFile;
        }
        return $fields;
    }
}
