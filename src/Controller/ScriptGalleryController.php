<?php

namespace App\Controller;

use App\Repository\GalleryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ScriptGalleryController extends AbstractController
{
    /**
     * @Route("/script/gallery", name="script_gallery")
     * @param GalleryRepository $galleryRepository
     * @return JsonResponse
     */
    public function gallery(GalleryRepository $galleryRepository)
    {
        $arrayCollection = $galleryRepository->findAll();
        $galleries = array();

        foreach ($arrayCollection as $item) {
            $galleries[] = $item->getImage();
        }
        return new JsonResponse($galleries);
    }
}
