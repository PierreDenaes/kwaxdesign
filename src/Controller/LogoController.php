<?php

namespace App\Controller;

use App\Repository\HomeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class LogoController extends AbstractController
{
    /**
     * @param Environment $twig
     * @param HomeRepository $homeRepository
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function index(Environment $twig, HomeRepository $homeRepository)
    {
        return new Response($twig->render('logo/index.html.twig', [
            'homes' => $homeRepository->findAll()
        ]));
    }
}
